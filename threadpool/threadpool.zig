const std = @import("std");

pub fn main() !void {
    // 初始化一个包含4个线程的线程池
    var pool: std.Thread.Pool = undefined;
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    try pool.init(.{ .allocator = allocator });
    defer pool.deinit();

    // 定义一个简单的任务函数

    // 提交几个任务到线程池中
    //const args = [_]u32{ 1, 2, 3, 4, 5 };
    for (1..10) |arg| {
        //const a: u32 = @intCast(arg);
        try pool.spawn(Task, .{@as(u32, @intCast(arg))});
    }

    // 自动等待所有任务完成

}
fn Task(arg: u32) void {
    std.debug.print("Task executed with argument: {d}\n", .{arg});
}
