const std = @import("std");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    //items1 和 items2 长度必须相等
    const items1 = [_]usize{ 1, 2, 3, 4 };
    const items2 = [_]usize{ 6, 7, 8, 9 };
    for (items1, items2) |i, j| {
        try stdout.print("i:{}, j:{}\n", .{ i, j });
    }
}
